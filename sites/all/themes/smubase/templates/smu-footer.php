
<div class="container">
  <div id="content-col3" class="content-col3 row">
    <?php if ($page['content_col3-1']): ?>
    <div class="col-md-4">
      <div id="content-col3-1" class=" content-col3"> <?php print render($page['content_col3-1']); ?> </div>
    </div>
    <?php endif; ?>
    <?php if ($page['content_col3-2']): ?>
    <div class="col-md-4">
      <div id="content-col3-2" class=" content-col3"> <?php print render($page['content_col3-2']); ?> </div>
    </div>
    <?php endif; ?>
    <?php if ($page['content_col3-3']): ?>
    <div class="col-md-4">
      <div id="content-col3-3" class=" content-col3"> <?php print render($page['content_col3-3']); ?> </div>
    </div>
    <?php endif; ?>
  </div>
  <?php if ($page['content_col3-1'] || $page['content_col3-2']  || $page['content_col3-3'] ): ?>
  <div class="content-col2-hr">
    <hr />
  </div>
  <?php endif; ?>
  <div class="content-col2">
    <div id="content-col2" class="row">
      <div class="col-md-4">
        <p id="smu-logo-footer"> <a href="/" rel="home" title="Singapore Management University (SMU)"><img id="smulogo" alt="Singapore Management University (SMU)" src="/sites/all/themes/smu/images/smu_logo.png"></a></p>
        <ul class="social-menu">
          <li class="twitter"><a target="_blank" class="twitter" title="SMU (@sgsmu) on Twitter" href="http://twitter.com/sgsmu"><i class="fa fa-twitter-square"></i><span class="menu-item">Twitter</span></a></li>
          <li class="facebook"><a target="_blank" class="facebook" title="SMU on Facebook" href="http://facebook.com/sgsmu"><i class="fa fa-facebook-square"></i><span class="menu-item">Facebook</span></a></li>
          <li class="linkedin"><a target="_blank" class="linkedin" title="SMU on LinkedIn" href="http://www.linkedin.com/company/singapore-management-university/"><i class="fa fa-linkedin-square"></i><span class="menu-item">LinkedIn</span></a></li>
          <li class="google"><a target="_blank" class="google" title="SMU on Google+" href="https://plus.google.com/u/0/113384635000139446323/"><i class="fa fa-google-plus-square"></i><span class="menu-item">Google+</span></a></li>
          <li class="flickr"><a target="_blank" class="flickr" href="http://www.flickr.com/sg-smu"><i class="fa fa-flickr"></i><span class="menu-item">Flickr</span></a></li>
          <li class="soundcloud"><a target="_blank" class="soundcloud" href="http://soundcloud.com/sgsmu"><i class="fa fa-soundcloud"></i><span class="menu-item">SoundCloud</span></a></li>
        </ul>
        <ul class="social-menu">
          <li class="youtube"><a target="_blank" class="youtube" title="SMU on YouTube" href="http://www.youtube.com/smutubesg"><i class="fa fa-youtube-square"></i><span class="menu-item">YouTube</span></a></li>
          <li class="itunesu"><a target="_blank" class="itunesu" href="http://itunes.smu.edu.sg/"><i class="fa fa-graduation-cap"></i><span class="menu-item">ITunes U</span></a></li>
          <li class="instragram"><a target="_blank" class="instragram" href="http://instagram.com/thesmushop"><i class="fa fa-instagram"></i><span class="menu-item">Instagram</span></a></li>
          <li class="pinterest"><a target="_blank" class="pinterest" href="http://www.pinterest.com/sgsmu/"><i class="fa fa-pinterest-square"></i><span class="menu-item">Pinterest</span></a></li>
          <li class="rss"><a target="_blank" class="rss" href="http://www.smu.edu.sg/rss"><i class="fa fa-rss-square"></i><span class="menu-item">Rss</span></a></li>
          <li class="smublog"><a target="_blank" class="smublog" href="http://blog.smu.edu.sg/"><i class="fa fa-pencil-square"></i><span class="menu-item">SMU Blog</span></a></li>
        </ul>
      </div>
      <?php if ($page['footer']): ?>
      <div class="col-md-4"> <?php print render($page['footer']); ?> </div>
      <?php endif; ?>
      <div class="col-md-4"> 
        <!-- smusearch-->
        <h2>LOOKING FOR SOMETHING?</h2>
        <form name="cse" id="searchbox" action="https://www.smu.edu.sg/search">
          <div class="input-group">
            <input name="query" type="text" class="form-control pull-right" placeholder="Search SMU..." />
            <div class="input-group-btn">
              <button type="submit" class="btn btn-default" 
                     tabindex="-1" style="border-radius:4px; margin-left:5px; background-image:linear-gradient(to bottom, #e0e0e0 0px, #e0e0e0 100%);" value="Search" name="sa"><span class="glyphicon glyphicon-search"></span> </button>
            </div>
            
            <!-- /btn-group --> 
          </div>
          
          <!-- /input-group -->
          
        </form>
        <!-- end smusearch--> 
      </div>
    </div>
  </div>
</div>
<div id="footer-bar">
  <div class="container">
    <div class="footer-bar-inner col-md-12 <?php print $row_fluid; ?>">
      <div id="copyright" class="pull-left"><a href="http://smu.edu.sg/terms-use" class="href"> Terms of Use</a> | <a href="http://smu.edu.sg/privacy-statement" class="href">Privacy Statement </a><br>
        &copy; Copyright <?php echo date ('Y');?> Singapore Management University. All Rights Reserved </div>
      <div class="pull-right hidden-xs">
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <div class="feedback"> <a href="http://www.facebook.com/sgsmu" title="Singapore Management University (SMU) on Facebook">SMU on Facebook</a> </div>
        <div class="fb-like" data-font="lucida grande" data-href="http://www.facebook.com/sgsmu" data-layout="button_count" data-send="false" data-show-faces="false" data-width="90"></div>
      </div>
    </div>
  </div>
</div>
<!-- end footer bar---> 
<div class="pull-right">
<a href="#" class="back-to-top"><img src="/sites/all/themes/smubase/images/uparr-48-b.png" /></a>
</div>