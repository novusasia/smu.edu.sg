<?php

/**
 * @file
 * Default theme implementation to display a block.
 *
 * Available variables:
 * - $block->subject: Block title.
 * - $content: Block content.
 * - $block->module: Module that generated the block.
 * - $block->delta: An ID for the block, unique within each module.
 * - $block->region: The block region embedding the current block.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - block: The current template type, i.e., "theming hook".
 *   - block-[module]: The module generating the block. For example, the user
 *     module is responsible for handling the default user navigation block. In
 *     that case the class would be 'block-user'.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $block_zebra: Outputs 'odd' and 'even' dependent on each block region.
 * - $zebra: Same output as $block_zebra but independent of any block region.
 * - $block_id: Counter dependent on each block region.
 * - $id: Same output as $block_id but independent of any block region.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $block_html_id: A valid HTML ID and guaranteed unique.
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 * @see template_process()
 *
 * @ingroup themeable
 */
  // Check domains             
    if(module_exists("domain")){            
  $domain = domain_get_domain();
  $domain_id = $domain['domain_id'];

  // Get profile type
  }
  else {
      $d = variable_get("smufaculty_school", "all");    
  }          
  $filter = $_GET['filter'];                             
  $profile_type = $filter['profile_type'][0];
  
?>
<!-- USING -->
<div class="faculty listings" id="faculty-top">

    <?php print render($title_prefix); ?>
    <div class="block-header">
      <?php if ($block->subject): ?>
        <h2<?php print $title_attributes; ?>><?php print $block->subject ?></h2>
        <div class="filter-display">
          <div class="selected" ng-show="filter.lastname.length">
          Last Name: <strong>{{filter.lastname}}</strong><div class="clear-lastname"><a ng-href="" ng-click="clear_lastname()" class="pointer">x</a></div>
          </div>
          <div class="selected-tags" ng-hide="loading || !ra_values_selected.length">
              You have selected:
              <span ng-show="check_all"><strong>All</strong> research areas.</span>
              <span ng-hide="check_all">
              <span ng-repeat="tag in ra_values_selected | limitTo:tag_limit" ng-class="{'last':$last}"><a href="/{{faculty_path}}?tid={{tag.tid}}" class="tag">{{tag.name}}</a></span><span ng-show="ra_values_length > 0"> and {{ra_values_selected.length - tag_limit}} other items .</span>
              </span>
          </div>
        </div>
        <div class="reset"><button ng-click="reset()">Reset</button></div>
      <?php endif;?>
        <?php if($d == 'all'): ?>
      <div class="back"><a href="/faculty/main">< Back to Faculty Main</a></div>
      <?php endif ?>
      <div class="clear"></div>
    </div>
    <?php print render($title_suffix); ?>

    <div class="content"<?php print $content_attributes; ?>>
      <div ng-show="loading">Loading profiles...</div>

      <div class="page-num" ng-show="items_total > 0 && !loading">
<!--       Page: {{page}}
      Length: {{profiles.length}}
      Items: {{items_per_page}}
      LimitTo: {{page - profiles.length}} -->
      Showing {{items_len_start}} - {{items_len_end}} of {{items_total}}
      </div>

      <div class="clear"></div>

      <div class="profile-listings" ng-hide="loading">

        <div class="no-profiles" ng-show="!profiles.length">No profiles found.</div>
        <ul class="clean" ng-show="profiles.length">
          <li class="profile profile-{{p.nid}}" ng-repeat="p in profiles | limitTo: (page - profiles.length) - page | limitTo: items_per_page">
            <div class="profile-left">
              <div class="photo">
              <a href="/faculty/profile/{{p.nid}}/{{p.node_title | encodename}}">
                <img src="{{p.email | parseThumbnailPhoto}}" ng-show="p.ntlogin.length == 0">
                <img src="{{p.ntlogin | parseThumbnailPhoto}}" ng-show="p.ntlogin.length > 0">
              </a>
              </div>
            </div>
            <div class="profile-right">
              <h3 class="title">
              <a href="/faculty/profile/{{p.nid}}/{{p.node_title | encodename}}">{{p.node_title}}</a>
                <div class="leave-status" ng-if="p.status == 'no'">
                  (On Leave)
                </div>
              </h3>
              <div class="field designation" ng-bind-html="p.designation" ng-show="p.designation"></div>
              <div class="field qualifications" ng-bind-html="p.qualifications" ng-show="p.qualifications"></div>

              <?php if($domain_id == 1): ?>
              <div class="field profile-types">
                <div class="label">Profile:</div>
                <div class="item">
                  <ul class="inline clean">
                    <li ng-repeat="t in p.profile_type track by $index" ng-class="{'last':$last}">{{t}}<span class="separator" ng-hide="$last">, </span></li>
                  </ul>
                </div>
              </div>
              <?php endif ?>

              <div class="field school" ng-show="p.school.length">
                <div class="label">School:</div>
                <div class="item">
                <a href="http://{{s | schoolify}}.smu.edu.sg/{{faculty_path}}" class="text-{{s | slugify}}" ng-class="{'last':$last}" ng-repeat="s in p.school">{{s}}</a>
                </div>
              </div>
              <div class="field research-areas" ng-show="p.research_areas_map.length">
                <div class="label" ng-hide="p.profile_type[0] == 'Adjunct Faculty' && p.school[0] == 'Lee Kong Chian School of Business'">Research Areas and Areas of Expertise:</div>
                <div class="label" ng-show="p.profile_type[0] == 'Adjunct Faculty' && p.school[0] == 'Lee Kong Chian School of Business'">Discipline:</div>
                <div class="clear"></div>
                <div class="item">
                  <ul class="inline clean">
                    <li ng-repeat="ra in p.research_areas_map" ng-hide="p.school[0] == 'Lee Kong Chian School of Business' && $index > 4">
                      <a href="/{{faculty_path}}?tid={{ra.tid}}<?php if($profile_type):?>&filter%5Bprofile_type%5D%5B%5D=<?php print $profile_type ?><?php endif ?>" class="tag" ng-bind-html="ra.name"></a>
                    </li>
                  </ul>
                  <a href class="show-more" ng-show="p.school[0] == 'Lee Kong Chian School of Business' && p.research_areas_map.length > 5" ng-click="showMoreTags(p, $event);">Show more</a>
                </div>
              </div>
              <div class="field contact">
                <div class="email" ng-show="p.email">
                  <div class="label">Email:</div>
                  <div class="item"><a href="mailto:{{p.email}}">{{p.email}}</a></div>
                </div>
                <div class="phone" ng-show="p.phone">
                  <div class="label">Phone:</div>
                  <div class="item">{{p.phone}}</div>
                </div>
              </div>
            </div>
<!--             Name: <a href="/faculty/profile/{{p.nid}}" target="_blank">{{p.node_title}}</a> ({{p.nid}})<br />
            Research Areas: {{p.field_profile_research_areas}}<br />
            Profile Type: {{p.profile_type}} -->
          </li>
        </ul>


        <div class="page-num" ng-show="items_total > 0 && !loading">
<!--         Page: {{page}}
        Length: {{profiles.length}}
        Items: {{items_per_page}}
        LimitTo: {{page - profiles.length}} -->
        Showing {{items_len_start}} - {{items_len_end}} of {{items_total}}
        </div>

        <div class="pagination">
          <a ng-href="#" ng-click="update_page($event, page - 1)" ng-show="page > 1">Previous</a>
          <a ng-href="#" ng-click="update_page($event, page + 1)" ng-show="page * items_per_page < items_total">Next</a>
        </div>
      </div>
    </div>


</div>
