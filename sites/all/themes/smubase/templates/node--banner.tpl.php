<?php
$wrapper = entity_metadata_wrapper('node', $node);
$formtype = field_get_items('node', $node, 'field_slide');
$field_slide_type = $node->field_slide_type['und']['0']['value'];
?>
<!-- masterslider -->

<div class="master-slider <?php if ($field_slide_type=='2'){echo "ms-skin-black-1";}elseif ($field_slide_type=='3'){echo "ms-skin-black-1";}else {echo "ms-skin-default";}?> " id="masterslider_<?php echo $field_slide_type;?>">
  <?php

foreach($formtype as $itemid) {
$item = field_collection_field_get_entity($itemid);
$bimage = file_create_url($item->field_image['und'][0]['uri']);
$bimage_w = $item->field_image['und'][0]['width'];
$bimage_h = $item->field_image['und'][0]['height'];
$bbody = $item->field_body_left['und'][0]['value'];
$blink = $item->field_link['und'][0]['url'];
$bvideo = $item->field_rsvp_link['und'][0]['url'];
$link_title = $item->field_link['und'][0]['title'];
 if (!empty($link_title)){
 $link_title = $link_title;
 }
 else{$link_title = 'Learn more';}
?>
  
  <!-- new slide -->
 <div class="ms-slide">
    
    <!-- slide background --> 
    <img src="#" data-src="<?php echo $bimage;?>" alt="<?php echo $btitle;?>"/> 
    
   
    <!-- slide text layer -->
    <div class="ms-layer ms-caption " data-delay="6"> 
    
    <?php if (!empty($bbody)):?>
	<?php echo $bbody;?> 
      <?php endif;?>
    
     <?php if (!empty($blink)):?>
    <a href="<?php echo $blink;?>" class="slider-button ms-btn"><?php echo $link_title;?></a> 
     <?php endif;?>
    </div>
    
     
    <?php if (!empty($bvideo)):?>
    <!-- youtube video --> 
    <a href="<?php echo $bvideo;?>" data-type="video">Youtube video</a>
     <?php endif;?>
       
    <?php
    if($field_slide_type==1) {?>
      <div class="ms-thumb"> 
        <img  src="<?php echo $bimage;?>" alt="<?php echo $btitle;?>"/> 
      </div>   
    <?php } ?>
    
  </div>
  <!-- end of slide -->
  
  <?php
}
?>
</div>
<!-- end of masterslider --> 
