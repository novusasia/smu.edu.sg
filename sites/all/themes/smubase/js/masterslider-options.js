// JavaScript Document

jQuery( document ).ready(function() {
	
	//normal slider
	 jQuery('#masterslider_3').masterslider({
    width: 996,
		height: 340,
        instantStartLayers:true,
        autoplay:true,
		loop: true,
         view:'basic',
		controls : {
			arrows : {autohide:true},
         //  thumblist: {autohide:true, display:false},
			bullets : {autohide:true, overVideo:true, dir:'h', align:'bottom', space:5 , margin:10,  inset:false}
		}
	});
	
	
	
   
   //partial slider
    jQuery('#masterslider_2').masterslider({
        width: 996,
        height: 350,
        loop: true,
        preload:0,
        instantStartLayers:true,
        hideLayers : false,
        speed:80,
        space:0,
        autoplay:true,
        startOnAppear:true,
       // view:'fadeBasic',
       view:'partialview',
        layout:'partialview',
        controls : {
            arrows : {autohide:false},
         	//thumblist: {autohide:true,display:false},
            bullets : {autohide:true, overVideo:true, dir:'h', align:'bottom', space:5 , margin:10,  inset:false }
                   }			
    });
	
	
	
	//full width with thumbanils slider
	 jQuery('#masterslider_1').masterslider({
        width: 996,
        height: 350,
        // more options...
        loop: true,
        instantStartLayers:true,
        fullwidth:true,
       preload:0,
        speed : 20,        
        space:0,
       // wheel:true,
        autoplay:true,
        startOnAppear:true,
        hideLayers : false,
       // speed : 100,
          //fillMode:'fit',
        controls : {
            arrows : {autohide:true},
            bullets : {autohide:true, overVideo:true, dir:'h', align:'bottom', space:5 , margin:10,  inset:false },
            thumblist : {
                autohide:false, 
                overVideo:true, 
                dir:'h', 
                speed:17, 
                inset:true, 
                arrows:false, 
                hover:false, 
                customClass:'slider_thumb', 
                align:'bottom',
                type:'thumbs', 
                margin:10, 
                width:57, 
                height:41, 
                space:5
            }        
            
    
            // more slider controls...
        }
    });
	

	
	
});