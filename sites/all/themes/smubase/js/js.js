
jQuery(document).ready(function ($) {
	
	/*Logo Switcher*/
		jQuery('.switcher > .switcher-trigger').popover({
			html : true,
			title: function() {
			  return jQuery(this).parent().find('.switcher-head').html();
			},
			content: function() {
			  return jQuery(this).parent().find('.switcher-content').html();
			},
			container: 'body',
			placement: 'bottom'
		});
		/*end Logo Switcher*/
		

/** MAIN MENU HOVER ON DESKTOP / CLICK ON MOBILE **/
jQuery(document).ready(function() {
	function bindNavbar() {
		if (jQuery(window).width() > 768) {
			jQuery('.nav .dropdown').on('mouseover', function(){         
				jQuery('.dropdown-toggle', this).next('.dropdown-menu').show().find('.dropdown-menu').hide();
			}).on('mouseout', function(){
				jQuery('.dropdown-toggle', this).next('.dropdown-menu').hide();
			});
			
			jQuery('a.dropdown-toggle').click(function() { 
					window.location = jQuery(this).attr('href');
			});
		}
		else {
			jQuery('.nav .dropdown').off('mouseover').off('mouseout');
			jQuery('a.dropdown-toggle').click(function(e) {
        e.preventDefault();
				if (jQuery(this).next('.dropdown-menu').is(':visible')) {
					window.location = jQuery(this).attr('href');
				}
			});  
      
		}
	}
	
	jQuery(window).resize(function() {
		bindNavbar();
	});
	
	bindNavbar();
});
/** MAIN MENU HOVER ON DESKTOP / CLICK ON MOBILE -- END **/	

/** SECONDARY MENU FIX **/        
jQuery('.region-sidebar-first .nav ul.dropdown-menu').removeClass('dropdown-menu');

jQuery('.region-sidebar-first .nav li.expanded').find('ul').each(function() {
  jQuery(this).addClass("side-dropdown-menu");
});	 

jQuery('.region-sidebar-first .nav li.expanded a').click(function() {
		if ((jQuery(this).next('.side-dropdown-menu').is(':visible') && !jQuery(this).hasClass('active')) || (!jQuery(this).next('.side-dropdown-menu').is(':visible') && !jQuery(this).hasClass('active'))) {
			window.location = jQuery(this).attr('href');
		}
    else {
      jQuery(this).next('.side-dropdown-menu').toggle();
    }
});	 

jQuery('.region-sidebar-first .nav li.expanded > span').click(function() {
      jQuery(this).next('.side-dropdown-menu').toggle();
});	
/** SECONDARY MENU FIX -- END **/

jQuery('ul.menu li > ul.side-dropdown-menu li > ul.side-dropdown-menu li .active').each(function() {
  jQuery(this).closest('.dropdown-submenu').addClass("open");
});

		 
//check for parent expanded menu and set is_visible class to it
var parent_menus = $('div.menu-block-wrapper > ul.menu > li').get();

if(parent_menus.length > 0){
	
	for(var i = 0; i < parent_menus.length; i++){

		
		if($(parent_menus[i]).hasClass('active-trail')){
			var parent_active_link = $(parent_menus[i]).children('a');
			var visible_parent_menu = $(parent_menus[i]).children('ul');

			if(visible_parent_menu.length > 0){
				$(parent_active_link).css('background','url("/sites/all/themes/smu/images/listing-filter-hover-down.png") no-repeat scroll right transparent');
			}else{
				$(parent_active_link).css('background','no-repeat scroll right center transparent');
			}
			
			//auto expand
			$(visible_parent_menu).slideDown(500).addClass('is_visible');
			$(visible_parent_menu).prev('a').css('cursor', 'pointer');
		}else{
			var first_level_menu = $(parent_menus[i]).parent('ul').parent('div.menu-block-wrapper');
			var social_media_menu = $(first_level_menu).hasClass("menu-block-ctools-menu-social-media-1").toString();

			//change background
			if(social_media_menu == 'false'){
				var children = $(parent_menus[i]).children('ul');
				if(children.length > 0){
					var parent_active_link = $(parent_menus[i]).children('a');
					$(parent_active_link).css('background','url("/sites/all/themes/smu/images/listing-filter-hover.png") no-repeat scroll right transparent');
				}
			}
			
		}
	}
}


//check for child expanded menu and set is_visible class to it
var child_menus = $('div.menu-block-wrapper > ul.menu > li > ul.menu > li').get();

if(child_menus.length > 0){
	
	for(var i = 0; i < child_menus.length; i++){

		if($(child_menus[i]).hasClass('active-trail')){
			var child_active_link = $(child_menus[i]).children('a');
			var visible_child_menu = $(child_menus[i]).children('ul');
			if(visible_child_menu.length > 0) {
				$(child_active_link).css('background','url("/sites/all/themes/smu/images/ico_arrow_right_down.png") no-repeat scroll right transparent');
			}else{

				var children = $(child_menus[i]).children('ul');
				if(children.length > 0){
					//change background image
					var child_active_link = $(child_menus[i]).children('a');
					$(child_active_link).css('background','url("/sites/all/themes/smu/images/ico_arrow_right.png") no-repeat scroll right transparent');
				}else{
					$(child_active_link).css('background','no-repeat scroll right center transparent');
				}
				
			}
			
			$(visible_child_menu).slideDown(500).addClass('is_visible');
			$(visible_child_menu).prev('a').css('cursor', 'pointer');
		}else{
			//change background image
			var children = $(child_menus[i]).children('ul');
			if(children.length > 0){
				//change background image
				var child_active_link = $(child_menus[i]).children('a');
				$(child_active_link).css('background','url("/sites/all/themes/smu/images/ico_arrow_right.png") no-repeat scroll right transparent');
			}
		}
	}
}

// left nav accordion
$('.menu-block-wrapper.menu-level-2 ul ul').each(function(){
  	if (!$(this).find('li.active').length>0) {
  		var parent = $(this).parent('li');
  		//if(!$(parent).hasClass('active-trail')){
		if(!$(parent).hasClass('active')){	
			$(this).hide();
  		}
  	}
});





$('div.menu-block-wrapper > ul.menu > li > ul.menu > li').click(function(event){

	var child_menu_to_change = $(this).children('ul');
	var link = $(this).children('a');

	if($(child_menu_to_change).hasClass('is_visible')){
		$(child_menu_to_change).slideUp(500).removeClass('is_visible');
		$(child_menu_to_change).prev('a').css('cursor', 'pointer');
	}else{

		$(child_menu_to_change).slideDown(500).addClass('is_visible');
		$(child_menu_to_change).prev('a').css('cursor', 'pointer');
	}


	if($(child_menu_to_change).hasClass('is_visible')){
		$(link).css('background','url("/sites/all/themes/smu/images/ico_arrow_right_down.png") no-repeat scroll right transparent');
	}else{
		if(child_menu_to_change.length > 0){
			$(link).css('background','url("/sites/all/themes/smu/images/ico_arrow_right.png") no-repeat scroll right transparent');
		}else{
			$(link).css('background','no-repeat scroll right center transparent');
		}
	}

	event.stopPropagation();
});


$('div.menu-block-wrapper > ul.menu > li > ul.menu > li > ul.menu > li').click(function(event){
	
	var child_menu_to_change = $(this).children('ul');

	if($(child_menu_to_change).hasClass('is_visible')){
		$(child_menu_to_change).slideDown(500).removeClass('is_visible');
		$(child_menu_to_change).prev('a').css('cursor', 'pointer');
	}else{

		$(child_menu_to_change).slideDown(500).addClass('is_visible');
		$(child_menu_to_change).prev('a').css('cursor', 'pointer');
	}

	event.stopPropagation();
});


//set font color for third level children menu
var third_level_link = $('div.menu-block-wrapper > ul.menu > li > ul.menu > li > ul.menu > li > a').get();
if(third_level_link.length > 0){
	for(var i = 0; i < third_level_link.length; i++){
		$(third_level_link[i]).css('color','#00467F');
		$(third_level_link[i]).css('background','no-repeat scroll right center transparent');
	}
}




	
//Bootstrap carousel
   
jQuery('.carousel-wrap .carousel[data-type="multi"] .item').each(function(){
	var next = $(this).next();
	if (!next.length) {
	next = $(this).siblings(':first');
	}
	next.children(':first-child').clone().appendTo($(this));
	
	for (var i=0;i<2;i++) {
	next=next.next();
	if (!next.length) {
		next = $(this).siblings(':first');
	}
	
	next.children(':first-child').clone().appendTo($(this));
	}
});

	
});

