<?php
function smubase_preprocess_html(&$vars) {
 	$path = request_path();
	$args = arg();
	// set up Javascript array of arguments
	// access with Drupal.settings.arg[];

   
  $node = menu_get_object();

  if ($node && $node->nid) {
    // allow customer html.tpl.php
    $vars['theme_hook_suggestions'][] = 'html__' . $node->type;
  } 
  
	// Typekit
	drupal_add_js('//use.typekit.com/uhs0req.js', array('type' => 'external'));
	drupal_add_js('try{Typekit.load();}catch(e){}', array('type' => 'inline'));
	
	//touchswipe
	drupal_add_js('/sites/all/libraries/jquery-touchswipe/jquery.touchSwipe.min.js', array('type' => 'external'));
	
	//jquery easing
	drupal_add_js('/sites/all/libraries/jquery.easing/jquery.easing.1.3.js', array('type' => 'external'));
  
  // jquery loading-overlay
  drupal_add_js('/sites/all/libraries/jquery-loading-overlay/dist/loading-overlay.min.js');
 
}

function smubase_preprocess_node(&$variables) {
  // Make "node--NODETYPE--VIEWMODE.tpl.php" templates available for nodes 
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['type'];
    $node =& $variables['node'];
     if($node->type == 'banner'){
		drupal_add_css('/sites/all/themes/smubase/css/banner.css',array('type' => 'external'));
		drupal_add_css('/sites/all/libraries/masterslider/style/masterslider.css',array('type' => 'external'));
		drupal_add_css('/sites/all/libraries/masterslider/skins/default/style.css',array('type' => 'external'));
		drupal_add_css('/sites/all/libraries/masterslider/skins/light-2/style.css',array('type' => 'external'));
		drupal_add_css('/sites/all/libraries/masterslider/skins/black-1/style.css',array('type' => 'external'));
        drupal_add_css('/sites/all/libraries/masterslider/skins/black-2/style.css',array('type' => 'external'));
       
		drupal_add_js('/sites/all/libraries/masterslider/masterslider.min.js', array('type' => 'external'));
		
		drupal_add_js('/'.path_to_theme() . '/js/masterslider-options.js', array('type' => 'external'));
	}
	if($node->type == 'newsletter'){
		drupal_add_css('/sites/all/themes/newsletter/css/style.css',array('type' => 'external','weight' =>999,'group'=>CSS_THEME));
	}
	
}


function smubase_js_alter(&$javascript) {
  // Update jquery version for non-administration pages  
  if (arg(0) != 'admin' && arg(0) != 'panels' && arg(0) != 'ctools'  && !(module_exists('jquery_update'))) {
    $jquery_file = drupal_get_path('theme', 'smubase') . '/js/jquery-1.9.0.js';
    $jquery_version = '1.9.0';
 
    $javascript['misc/jquery.js']['data'] = $jquery_file;
    $javascript['misc/jquery.js']['version'] = $jquery_version;
    $javascript['misc/jquery.js']['weight'] = 0;
    $javascript['misc/jquery.js']['group'] = -101;
    
  }
}



function smubase_preprocess_page(&$vars) { 
  // theme option variables
  $vars['bootstrap_version'] = theme_get_setting('bootstrap_version');
  $vars['font_awesome_version'] = theme_get_setting('font_awesome_version'); 
  $vars['page_layout'] = theme_get_setting('page_layout'); 

  // Bootstrap
  $bootstrap_version = theme_get_setting('bootstrap_version');
  $font_awesome_version =theme_get_setting('font_awesome_version'); 

  if ($bootstrap_version == 'bootstrap-3') {
  drupal_add_css(path_to_theme() . '/packages/bootstrap/css/bootstrap.min.css', array('group' => CSS_DEFAULT, 'media' => 'all', 'weight' => 500, 'preprocess' => TRUE));
  
  drupal_add_css(path_to_theme() . '/packages/bootstrap/css/bootstrap-theme.min.css', array('group' => CSS_DEFAULT, 'media' => 'all', 'weight' => 500, 'preprocess' => TRUE));
  
  drupal_add_js(path_to_theme() . '/packages/bootstrap/js/bootstrap.min.js');
  
  }

  if ($font_awesome_version == 'font-awesome-4') {
  drupal_add_css(path_to_theme() . '/packages/font-awesome-4.1.0/css/font-awesome.min.css', array('group' => CSS_DEFAULT, 'media' => 'all', 'weight' => 500, 'preprocess' => TRUE));
  }

}
function smubase_menu_tree($variables) {
  return '<ul class="menu nav navbar-nav">' . $variables['tree'] . '</ul>';
}
function smubase_menu_tree__menu_block(&$variables) {
  return '<ul class="menu nav">' . $variables['tree'] . '</ul>';
}