<div class="container">
  <div class="col-md-2">
    <h3>Programmes</h3>
    <ul>
      <li><a href="http://www.smu.edu.sg/programmes/undergraduate">Undergraduate</a> </li>
      <li><a href="http://www.smu.edu.sg/programmes/postgraduate">Postgraduate</a> </li>
      <li><a href="http://www.smu.edu.sg/programmes/doctoral">Doctoral</a> </li>
      <li><a href="http://www.smu.edu.sg/executive-development">Executive Education</a></li>
      <li><a href="http://www.smu.edu.sg/programmes/professional">Professional</a> </li>
      <li><a href="http://www.smu.edu.sg/admissions/">Admissions</a> </li>
      <li><a href="http://info.smu.edu.sg/courses/">Course Catalog</a></li>
    </ul>
  </div>
  <div class="col-md-2">
    <h3>Research</h3>
    <ul>
      <li><a href="http://www.smu.edu.sg/area-of-excellence">Area of Excellence</a> </li>
      <li><a href="http://www.smu.edu.sg/perspectives">Perspectives@SMU</a> </li>
      <li><a href="http://casewriting.smu.edu.sg/">Case Writing</a> </li>
      <li><a href="http://www.smu.edu.sg/publications">Publications</a> </li>
      <li><a href="http://www.smu.edu.sg/research-smu">Research Office</a> </li>
      <li><a href="http://www.smu.edu.sg/topics">Topics</a></li>
    </ul>
  </div>
  <div class="col-md-2">
    <h3>Academics</h3>
    <ul>
      <li><strong>Schools</strong>
        <ul>
          <li><a href="http://accountancy.smu.edu.sg">Accountancy</a></li>
          <li><a href="http://business.smu.edu.sg">Business</a></li>
          <li><a href="http://economics.smu.edu.sg">Economics</a></li>
          <li><a href="http://sis.smu.edu.sg">Information Systems</a></li>
          <li><a href="http://law.smu.edu.sg">Law</a></li>
          <li><a href="http://socsc.smu.edu.sg">Social Sciences</a></li>
        </ul>
      </li>
      <li><a href="http://library.smu.edu.sg">Library</a></li>
      <li><a href="http://www.smu.edu.sg/smu/institutes">Institutes</a></li>
      <li><a href="http://www.smu.edu.sg/smu/centres">Centres</a></li>
      <li><a href="http://www.smu.edu.sg/smu/labs-initiatives">Labs &amp; Initiatives</a></li>
    </ul>
  </div>
  <div class="col-md-2">
    <h3>About</h3>
    <ul>
      <li><a href="http://www.smu.edu.sg/about">About SMU</a> </li>
      <li><a href="http://www.smu.edu.sg/directory">Directory</a></li>
      <li><a href="http://www.smu.edu.sg/news">News</a></li>
      <li><a href="http://www.smu.edu.sg/events">Events</a></li>
      <li><a href="http://www.smu.edu.sg/tuition-fees">Fees and Financial Aid</a></li>
      <li><a href="http://www.smu.edu.sg/scholarships-and-awards">Scholarships and Awards</a></li>
      <li><a href="http://www.smu.edu.sg/careers-overview">Careers</a></li>
      <li><a href="http://studentlife.smu.edu.sg/">Student Life</a></li>
      <li><a href="http://www.smu.edu.sg/education-support">Education Support</a></li>
    </ul>
  </div>
  <div class="col-md-2">
    <h3>Info For</h3>
    <ul>
      <li><a href="http://www.smu.edu.sg/info-for/prospective-students/">Prospective Students</a> </li>
      <li><a href="http://www.smu.edu.sg/info-for/current-students/">Current Students</a> </li>
      <li><a href="http://www.smu.edu.sg/info-for/faculty-staff/">Faculty &amp; Staff</a> </li>
      <li><a href="http://www.smu.edu.sg/info-for/alumni/">Alumni</a> </li>
      <li><a href="http://www.smu.edu.sg/info-for/media/">Media</a> </li>
      <li><a href="http://www.smu.edu.sg/info-for/parents-visitors/">Parents &amp; Visitors</a> </li>
      <li><a href="http://www.smu.edu.sg/info-for/employers/">Employers</a></li>
    </ul>
  </div>
  <div class="col-md-2">
    <h3>Login To</h3>
    <ul>
      <li><strong>Email</strong>
        <ul>
          <li><a href="https://live.smu.edu.sg/" target="blank">Students</a></li>
          <li><a href="https://email.smu.edu.sg/" target="blank">Staff/Faculty</a></li>
        </ul>
      <li><a href="https://ewise.smu.edu.sg" target="blank">eWISE intranet</a></li>
      <li><a href="https://oasis.smu.edu.sg" target="blank">OASIS intranet</a></li>
      <li><a href="http://elearn.smu.edu.sg" target="blank">eLearn</a></li>
      <li><a href="https://extranet.smu.edu.sg/" target="blank">SMU Extranet</a></li>
      <li><a href="https://singapore-management-csm.symplicity.com/" target="blank">OnTrac II</a></li>
    </ul>
  </div>
  <div class="col-md-12 clearfix"><p><a href="http://www.smu.edu.sg">SMU Home</a></p></div>
</div>
