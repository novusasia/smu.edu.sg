<a href="#main" class="element-invisible element-focusable"><?php print t('Skip to content'); ?></a>
<?php if ($main_menu): ?>
<a href="#main-nav" class="element-invisible element-focusable" data-target=".nav-collapse" data-toggle="collapse"><?php print t('Skip to navigation'); ?></a>
<?php endif; ?>
<!-- /#skipnav -->
<?php if ((($user->uid) && ($page['admin_shortcuts'])) || (($user->uid) && ($secondary_nav))): ?>
<div id="admin-shortcuts" class="admin-shortcuts clearfix"> <?php print render($secondary_nav); ?> <?php print render($page['admin_shortcuts']); ?> </div>
<?php endif; ?>

<!-- old header -->

<?php if (($main_menu) || ($page['search_box'])): ?>
<div id="main-menu" class="clearfix site-main-menu">
  <div id="slide-menu-wrapper" class="hidden-xs">
    <div id="menu-global" class="collapse slide-menu">
      <?php

	include('slidemenu.php');
?>
    </div>
    <p class="btn-slide pull-right" data-toggle="collapse" data-target="#menu-global">Main Menu</p>
  </div>
  
  <!-- /#top menu-->
  <?php if ($page['top_menu']): ?>
  <div class="container">
    <div id="top-menu" class="top-menu hidden-xs <?php print $row_fluid; ?>"> <?php print render($page['top_menu']); ?> </div>
  </div>
  <?php endif; ?>
  <!-- end /#top menu-->
  
  <div class="container">
    <?php if ($bootstrap_version == "bootstrap-3"): ?>
    <nav class="<?php print $row_fluid; ?>" role="navigation">
      <div class="navbar navbar-inverse navbar-static-top" data-role="navigation">
        <div class="navbar-inner-cus">
          <div id="nav-logo" class="col-md-6 col-sm-6">
            <?php if ($logo): ?>
            <span class="smu-logo  hidden-xs"> <a target="_blank" href="http://www.smu.edu.sg" rel="home" title="Singapore Management University (SMU)" name="top"> <img id="smulogo" alt="Singapore Management University (SMU)" src="/sites/all/themes/smubase/smu_logo.png"> </a></span> <span class="hidden-sm hidden-md hidden-lg"> <a target="_blank" href="http://www.smu.edu.sg" rel="home" title="Singapore Management University (SMU)" name="top"> <img id="smulogo" alt="Singapore Management University (SMU)" src="/sites/all/themes/smubase/smulogo_mobile.png?update"> </a> </span> <span id="logo"> <a href="<?php if(arg(0) == 'perspectives'){print 'http://cmp.smu.edu.sg';}else{print $front_page;} ?>" title="<?php print t('Home'); ?>" rel="home"> <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" role="presentation" /> </a>
            <?php if ($page['top_logo']): ?>
            <?php print render($page['top_logo']); ?>
            <?php endif; ?>
            </span>
            <?php endif; ?>
            <!-- /#logo -->
            
            <?php if (!empty($page['switcher'])): ?>
            <span class="switcher"> <a href="#" class="switcher-trigger">This link triggers the switcher</a> 
            <!-- <div class="switcher-head hide">Switch Site</div>-->
            <div class="switcher-content hide"><?php print render($page['switcher']); ?></div>
            </span>
            <?php endif; ?>
            <button type="button" class="navbar-toggle pull-right collapsed" data-toggle="collapse" data-target="#smu-navbar-collapse-nav"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <button type="button" class="navbar-toggle navbar-right right pull-right collapsed navbar-link glyphicon glyphicon-search" data-toggle="collapse" data-target="#smu-navbar-collapse-search"> </button>
          </div>
          <!-- navbar-left-->
          <div id="nav-search" class="col-md-6 col-sm-6">
            <div class="collapse navbar-collapse" id="smu-navbar-collapse-search">
              <?php if ($page['search_box']): ?>
              <?php print render($page['search_box']); ?>
              <?php endif; ?>
            </div>
          </div>
          <div class="collapse navbar-collapse navbar-left" id="smu-navbar-collapse-nav">
            <div class="nav navbar-nav">
              <?php if (($primary_nav) && empty($page['navigation'])): ?>
              <?php print render($primary_nav); ?> 
              <!-- /#primary-menu -->
              <?php endif; ?>
              <?php if (!empty($page['navigation'])): ?>
              <?php print render($page['navigation']); ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
        <!--navbar-static-top--> 
      </div>
      <!-- navbar-inner--> 
    </nav>
    <?php endif; ?>
  </div>
  <!-- container--> 
</div>
<!-- /#main-menu -->
<?php endif; ?>
<?php if ($page['main_top']): ?>
<div class="container-fluid">
  <div id="main-top" class="clearfix main-top <?php print $row_fluid; ?>" ><?php print render($page['main_top']); ?></div>
</div>
<?php endif; ?>
<?php if ($page['main_upper']): ?>
<div id="main-upper">
  <div class="container">
    <div class="main-upper <?php print $row_fluid; ?>"><?php print render($page['main_upper']); ?> </div>
  </div>
</div>
<?php endif; ?>
<div id="main" class="clearfix main" role="main">
  <div class="container">
    <div id="main-content" class="main-content <?php print $row_fluid; ?>"> <?php print render($title_prefix); ?>
      <?php if (!($is_front) && ($breadcrumb)): ?>
      <div class="breadcrumb"> <?php print $breadcrumb; ?> </div>
      <?php endif; ?>
      <?php if ($title): ?>
      <div class="page-title">
        <h1 class="title" id="page-title"> <?php print $title; ?> </h1>
      </div>
      <?php endif; ?>
      <?php if ($page['sidebar_first']): ?>
      <div id="sidebar-first" class="sidebar <?php if ($page_layout == "sidebar-4col"): print $part4; else: print $part3; endif; ?> site-sidebar-first">
        <div class="row"> <?php print render($page['sidebar_first']); ?> </div>
      </div>
      <!-- /#sidebar-first -->
      <?php endif; ?>
      <div id="content" class="content <?php if (($page_layout == "sidebar-4col") && (($page['sidebar_first']) && ($page['sidebar_second']))): print $part4; elseif (($page_layout == "sidebar-4col") && (($page['sidebar_first']) || ($page['sidebar_second']))): print $part8; elseif (($page['sidebar_first']) && ($page['sidebar_second'])): print $part6; elseif (($page['sidebar_first']) || ($page['sidebar_second'])): print $part9; else: print $part12; endif; ?>">
        <div id="content-wrapper" class="content-wrapper row">
          <div id="content-head" class="content-head">
            <div class="clearfix clear-row"> <?php print render($title_suffix); ?>
              <?php if ($messages): ?>
              <div id="console" class="clearfix"><?php print $messages; ?></div>
              <?php endif; ?>
              <?php if (isset($tabs['#primary'][0]) || isset($tabs['#secondary'][0])): ?>
              <div class="tabs"> <?php print render($tabs); ?> </div>
              <?php endif; ?>
              <?php if ($page['highlighted']): ?>
              <div id="highlighted" class="clearfix"><?php print render($page['highlighted']); ?></div>
              <?php endif; ?>
              <?php if ($page['help']): ?>
              <div id="help" class="clearfix"> <?php print render($page['help']); ?> </div>
              <?php endif; ?>
              <?php if ($action_links): ?>
              <ul class="action-links">
                <?php print render($action_links); ?>
              </ul>
              <?php endif; ?>
            </div>
          </div>
          <?php if ($page['content_top']): ?>
          <div id="content-top" class=" content-top"> <?php print render($page['content_top']); ?> </div>
          <?php endif; ?>
          <?php if ($page['content_upper']): ?>
          <div id="content-upper" class="content-upper"> <?php print render($page['content_upper']); ?> </div>
          <?php endif; ?>
          <?php if (($page['content']) || ($feed_icons)): ?>
          <div id="content-body" class="content-body"> <?php print render($page['content']); ?> <?php print $feed_icons; ?>
          </div>
          <?php endif; ?>
          <?php if ($page['content_row4']): ?>
          <div id="content-row4" class=" content-row4"> <?php print render($page['content_row4']); ?> </div>
          <?php endif; ?>
          <?php if (($page['content_col4-1']) || ($page['content_col4-2']) || ($page['content_col4-3']) || ($page['content_col4-4'])): ?>
          <div id="content-col4" class="  content-col4">
            <?php if ($page['content_col4-1']): ?>
            <div class="<?php print $part3; ?>">
              <div id="content-col4-1" class=" content-col4"> <?php print render($page['content_col4-1']); ?> </div>
            </div>
            <?php endif; ?>
            <?php if ($page['content_col4-2']): ?>
            <div class="<?php print $part3; ?>">
              <div id="content-col4-2" class="content-col4"> <?php print render($page['content_col4-2']); ?> </div>
            </div>
            <?php endif; ?>
            <?php if ($page['content_col4-3']): ?>
            <div class="<?php print $part3; ?>">
              <div id="content-col4-3" class=" content-col4"> <?php print render($page['content_col4-3']); ?> </div>
            </div>
            <?php endif; ?>
            <?php if ($page['content_col4-4']): ?>
            <div class="<?php print $part3; ?>">
              <div id="content-col4-4" class="content-col4"> <?php print render($page['content_col4-4']); ?> </div>
            </div>
            <?php endif; ?>
          </div>
          <?php endif; ?>
          <?php if ($page['content_lower']): ?>
          <div id="content-lower" class="content-lower"> <?php print render($page['content_lower']); ?> </div>
          <?php endif; ?>
          <?php if ($page['content_bottom']): ?>
          <div id="content-bottom" class="content-bottom"> <?php print render($page['content_bottom']); ?> </div>
          <?php endif; ?>
        </div>
        <!-- /#content-wrap --> 
      </div>
      <!-- /#content -->
      <?php if ($page['sidebar_second']): ?>
      <div id="sidebar-second" class="sidebar <?php if ($page_layout == "sidebar-4col"): print $part4; else: print $part3; endif; ?> site-sidebar-second"> <?php print render($page['sidebar_second']); ?></div>
      
      <!-- /#sidebar-second -->
      <?php endif; ?>
    </div>
  </div>
</div>
<!-- /#main, /#main-wrapper -->
<?php if ($page['main_lower']): ?>
<div id="main-lower" class="main-lower">
  <div class="container"> <?php print render($page['main_lower']); ?></div>
</div>
<?php endif; ?>
<?php if ($page['main_bottom-2']): ?>
<div id="main-bottom-2" class="main-bottom-2">
  <div class="container"> <?php print render($page['main_bottom-2']); ?> </div>
</div>
<?php endif; ?>
<?php if ($page['main_bottom-3']): ?>
<div id="main-bottom-3" class="main-bottom-3">
  <div class="container"> <?php print render($page['main_bottom-3']); ?> </div>
</div>
<?php endif; ?>
<?php if ($page['main_bottom-4']): ?>
<div id="main-bottom-4" class="main-bottom-4">
  <div class="container"> <?php print render($page['main_bottom-4']); ?> </div>
</div>
<?php endif; ?>
<div class="container-fluid">
  <div id="footer" class="<?php print $row_fluid; ?> site-footer" role="contentinfo">
    <?php include('smu-footer.php');?>
  </div>
</div>
<!-- /#footer --> 
