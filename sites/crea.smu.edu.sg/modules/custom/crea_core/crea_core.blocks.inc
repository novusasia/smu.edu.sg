<?php
/**
 * @file
 * crea_core.blocks.inc
 */

/**
 * Implements hook_block_info().
 */
function crea_core_block_info() {

  $blocks['crea_staticbanner'] = array(
    'info' => t('CREA Static Banner'),
  );

  $blocks['crea_contact'] = array(
    'info' => t('Contact Us'),
  );

  $blocks['crea_about'] = array(
    'info' => t('What is CREA?'),
  );

  $blocks['crea_msg'] = array(
    'info' => t('Message from the director'),
  );

  return $blocks;
}
 
/**
 * Implements hook_block_view().
 */
function crea_core_block_view($delta = '') {
  
  $block = array();

  switch ($delta) {
    case 'crea_staticbanner':
      $default_content = crea_default_value('crea_staticbanner_text_format');
      $block['content'] =  variable_get('crea_staticbanner_text_format', $default_content);
      $block['featured_image'] =  crea_get_image(variable_get('crea_staticbanner_image', ''), 
                                    'banner', 'crea_staticbanner');
      break;
    case 'crea_contact':
      $block['subject'] = t('Contact Us');
      $block['content'] =  variable_get('crea_contact_text_format', 
                        crea_default_value('crea_contact_text_format'));
      break;
    case 'crea_about':
      $node = _crea_get_node_by_title('About CREA', 'page');
      $nid = $node ? $node->nid : '';
      $block['subject'] = t('What is CREA?');
      $default_content = crea_default_value('crea_about_text_format');
      $block['content'] =  variable_get('crea_about_text_format', $default_content);
      $block['featured_image'] =  crea_get_image(variable_get('crea_about_image', ''), 'medium', 'crea_about');
      $block['link_label'] =  variable_get('crea_about_link_label', 'Read More');
      $block['link_url'] = variable_get('crea_about_link_url', 'node/' . $nid);
      break;
    case 'crea_msg':
      $node = _crea_get_node_by_title('Message from the Director', 'page');
      $nid = $node ? $node->nid : '';
      $block['subject'] = t('Message from the director');
      $default_content = crea_default_value('crea_msg_text_format');
      $block['content'] =  variable_get('crea_msg_text_format', $default_content);
      $block['featured_image'] =  crea_get_image(variable_get('crea_msg_image', ''), 'medium', 'crea_msg');
      $block['link_label'] =  variable_get('crea_msg_link_label', 'Read More');
      $block['link_url'] = variable_get('crea_msg_link_url', 'node/' . $nid);
      break;
    default:
      break;
  }

  return $block;
}
 
/**
 * Implements hook_block_configure().
 */
function crea_core_block_configure($delta = '') {
  $form = array();
  switch ($delta) {
    case 'crea_about':

    $image = crea_get_image(variable_get('crea_about_image', ''), 'thumbnail');

    if (!empty($image)) {
      $form['image_preview'] = array(
        '#type' => 'item',
        '#markup' => $image,
      );
    }

    $form['crea_about_image'] = 
      array(  '#type' => 'managed_file',
        '#title' => t('Featured image'), 
        '#description' => t('Allowed extensions: gif png jpg jpeg'),
        '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
          'file_validate_size' => array(1 * 1024 * 1024),
        ),
      '#default_value' => variable_get('crea_about_image', ''),
      '#upload_location' => 'public://crea/',
    );

      $default_content = crea_default_value('crea_about_text_format');
    $form['crea_about_text_format'] = array(
      '#type' => 'text_format',
      '#title' => t('Content'),
      '#default_value' => variable_get('crea_about_text_format', $default_content)
    );

    $form['crea_about_link_fieldset'] = array(
      '#type' => 'fieldset', 
      '#title' => 'Read more link',
      '#collapsible' => FALSE, 
      '#collapsed' => TRUE,
    );

    $form['crea_about_link_fieldset']['crea_about_link_label'] = array(
      '#type' => 'textfield', 
        '#title' => t('label'),
        '#default_value' => variable_get('crea_about_link_label', 'Read More')
    );

    $node = _crea_get_node_by_title('About CREA', 'page');
    $nid = $node ? $node->nid : '';
    $form['crea_about_link_fieldset']['crea_about_link_url'] = array(
      '#type' => 'textfield', 
        '#title' => t('URL'),
        '#default_value' => variable_get('crea_about_link_url', 'node/' . $nid)
    );

    break;

  case 'crea_msg':

    $image = crea_get_image(variable_get('crea_msg_image', ''), 'thumbnail');

    if (!empty($image)) {
      $form['image_preview'] = array(
        '#type' => 'item',
        '#markup' => $image,
      );
    }

    $form['crea_msg_image'] = 
      array(  '#type' => 'managed_file',
        '#title' => t('Featured image'), 
        '#description' => t('Allowed extensions: gif png jpg jpeg'),
        '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
          'file_validate_size' => array(1 * 1024 * 1024),
        ),
      '#default_value' => variable_get('crea_msg_image', ''),
      '#upload_location' => 'public://crea/',
    );

      $default_content = crea_default_value('crea_msg_text_format');
    $form['crea_msg_text_format'] = array(
      '#type' => 'text_format',
      '#title' => t('Content'),
      '#default_value' => variable_get('crea_msg_text_format', $default_content)
    );

    $form['crea_msg_link_fieldset'] = array(
      '#type' => 'fieldset', 
      '#title' => 'Read more link',
      '#collapsible' => FALSE, 
      '#collapsed' => TRUE,
    );

    $form['crea_msg_link_fieldset']['crea_msg_link_label'] = array(
      '#type' => 'textfield', 
        '#title' => t('label'),
        '#default_value' => variable_get('crea_msg_link_label', 'Read More')
    );


    $node = _crea_get_node_by_title('Message from the Director', 'page');
    $nid = $node ? $node->nid : '';
    $form['crea_msg_link_fieldset']['crea_msg_link_url'] = array(
      '#type' => 'textfield', 
        '#title' => t('URL'),
        '#default_value' => variable_get('crea_msg_link_url', 'node/' . $nid)
    );

    break;

  case 'crea_contact':
      $default_content = crea_default_value('crea_contact_text_format');
    $form['crea_contact_text_format'] = array(
      '#type' => 'text_format',
      '#title' => t('Content'),
      '#default_value' => variable_get('crea_contact_text_format', $default_content)
    );
    break;

  case 'crea_staticbanner':


    $image = crea_get_image(variable_get('crea_staticbanner_image', ''), 'thumbnail');

    if (!empty($image)) {
      $form['image_preview'] = array(
        '#type' => 'item',
        '#markup' => $image,
      );
    }

    $form['crea_staticbanner_image'] = 
      array(  '#type' => 'managed_file',
        '#title' => t('Featured image'), 
        '#description' => t('Allowed extensions: gif png jpg jpeg'),
        '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
          'file_validate_size' => array(1 * 1024 * 1024),
        ),
      '#default_value' => variable_get('crea_staticbanner_image', ''),
      '#upload_location' => 'public://crea/',
    );

    $default_content = crea_default_value('crea_staticbanner_text_format');
    $form['crea_staticbanner_text_format'] = array(
      '#type' => 'text_format',
      '#title' => t('Content'),
      '#default_value' => variable_get('crea_staticbanner_text_format', $default_content)
    );
    break;
  }
  return $form;
}
 
/**
 * Implements hook_block_save().
 */
function crea_core_block_save($delta = '', $edit = array()) {
  watchdog('error', print_r($edit, TRUE));
  switch ($delta) {
    case 'custom_block_with_custom_fields':   
    variable_set('custom_textfield', $edit['custom_textfield']);
    variable_set('custom_text_format', $edit['custom_text_format']['value']);
    break;
  case 'crea_about':   
    variable_set('crea_about_text_format', $edit['crea_about_text_format']['value']);
    variable_set('crea_about_image', $edit['crea_about_image']);
    variable_set('crea_about_link_label', $edit['crea_about_link_label']);
    variable_set('crea_about_link_url', $edit['crea_about_link_url']);
    break;
  case 'crea_msg':   
    variable_set('crea_msg_text_format', $edit['crea_msg_text_format']['value']);
    variable_set('crea_msg_image', $edit['crea_msg_image']);
    variable_set('crea_msg_link_label', $edit['crea_msg_link_label']);
    variable_set('crea_msg_link_url', $edit['crea_msg_link_url']);
    break;
  case 'crea_contact':   
    variable_set('crea_contact_text_format', $edit['crea_contact_text_format']['value']);
    break;
  case 'crea_staticbanner':   
    variable_set('crea_staticbanner_text_format', $edit['crea_staticbanner_text_format']['value']);
    variable_set('crea_staticbanner_image', $edit['crea_staticbanner_image']);
    break;
  }
}
