<?php
/**
 * @file
 * crea_core.blocks.inc
 */

function crea_get_image($fid, $image_style, $caller='') {

  $image = '';
  $uri = '';

  $file = file_load($fid);
  if ($file) {
    $uri = $file->uri;
    $url = file_create_url($uri);
    $image = theme('image_style',array('style_name' => $image_style, 'path' => $uri));
  } else {
    $image_url = 'http://dummyimage.com/600x400/ededed/000000';
    global $base_url;
    $module_path = drupal_get_path('module', 'crea_core');

    switch($image_style) {
      case 'medium':
        $image_url = $base_url . '/' . $module_path . '/images/what-is-crea.jpg';
        break;
      case 'thumbnail':
        $image_url = 'http://dummyimage.com/100x100/ededed/000000';
        break;
      case 'large':
      case 'banner':
        $image_url = $base_url . '/' . $module_path . '/images/homepage-banner.jpg';
        break;
      default:
        break;
    }

    switch($caller) {
      case 'crea_msg':
        $image_url = $base_url . '/' . $module_path . '/images/msg-image.png';
        break;
      default:
        break;

    }
    $image = '<img src="' . $image_url . '" />';
  }



  return $image;
}

function crea_default_value($id = '') {
  global $base_url;
  $module_path = $base_url . '/' . drupal_get_path('module', 'crea_core');

  $value = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique lectus nunc, nec suscipit felis sollicitudin commodo. Nulla euismod eget elit sed euismod. Sed auctor sagittis lacus, in finibus libero fermentum non. Integer vulputate erat dolor, ac congue nibh consectetur hendrerit. Nulla euismod eget elit sed euismod. Sed auctor sagittis lacus, in finibus libero fermentum non. Integer vulputate erat dolor, ac congue nibh consectetur hendrerit. Nulla euismod eget elit sed euismod. Sed auctor sagittis lacus, in finibus libero fermentum non. Integer vulputate erat dolor, ac congue nibh consectetur hendrerit. Nulla euismod eget elit sed euismod. Sed auctor sagittis lacus, in finibus libero fermentum non. Integer vulputate erat dolor, ac congue nibh consectetur hendrerit.</p>';

  switch($id) {
    case 'crea_about_text_format':
      $value = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique lectus nunc, nec suscipit felis sollicitudin commodo. Nulla euismod eget elit sed euismod. Sed auctor sagittis lacus, in finibus libero fermentum non. Integer vulputate erat dolor, ac congue nibh consectetur hendrerit. Nulla euismod eget elit sed euismod. Sed auctor sagittis lacus, in finibus libero fermentum non. Integer vulputate erat dolor, ac congue nibh consectetur hendrerit. Nulla euismod eget elit sed euismod. Sed auctor sagittis lacus, in finibus libero fermentum non. Integer vulputate erat dolor, ac congue nibh consectetur hendrerit. Nulla euismod eget elit sed euismod. Sed auctor sagittis lacus, in finibus libero fermentum non. Integer vulputate erat dolor, ac congue nibh consectetur hendrerit.</p>';
      break;

    case 'crea_msg_text_format':
      $value = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tristique lectus nunc, nec suscipit felis sollicitudin commodo. Nulla euismod eget elit sed euismod. Sed auctor sagittis lacus, in finibus libero fermentum non. Integer vulputate erat dolor, ac congue nibh consectetur hendrerit. Nulla euismod eget elit sed euismod. Sed auctor sagittis lacus, in finibus libero fermentum non. Integer vulputate erat dolor, ac congue nibh consectetur hendrerit. Integer vulputate erat dolor, ac congue nibh consectetur hendrerit. Nulla euismod eget elit sed euismod. Sed auctor sagittis lacus, in finibus libero fermentum non. Integer vulputate erat dolor, ac congue nibh consectetur hendrerit. Integer vulputate erat dolor, ac congue nibh consectetur hendrerit. Nulla euismod eget elit sed euismod. Sed auctor sagittis lacus, in finibus libero fermentum non. Integer vulputate erat dolor, ac congue nibh consectetur hendrerit.</p>';
      break;
    case 'crea_staticbanner_text_format':
      $value = '<h1>Welcome to the <br /> center for research <br /> on the economics of ageing</h1>';
      break;
    case 'crea_contact_text_format':
    $value = '
        <div>
        <h6><strong>CENTRE FOR RESEARCH ON THE <br /> ECONOMICS OF AGEING</strong></h6>
        <table border="0" cellpadding="1" cellspacing="1" class="noborders">
          <tbody>
            <tr>
              <td><img alt="" src="'.$module_path.'/images/list-style-home.png" /></td>
              <td>
              <div>Singapore Management University</div>
              <div>90 Stamford Road</div>
              <div>Singapore 178903</div>
              </td>
            </tr>
            <tr>
              <td><img alt="" src="'.$module_path.'/images/list-style-phone.png" /></td>
              <td>
              <div>Tel: +65 6808 7910</div>
              </td>
            </tr>
            <tr>
              <td><img alt="" src="'.$module_path.'/images/list-style-email.png" /></td>
              <td><a href="mailto:crea@smu.edu.sg">crea@smu.edu.sg</a></td>
            </tr>
          </tbody>
        </table>
        </div>

        <p>&nbsp;</p>';
      break;

    default:
      break;
  }

  return $value;
}

function _crea_generate_pages() {
  
  $body = file_get_contents('http://loripsum.net/api/10/long/decorate/bq/ul/ol/link/prude');
  $contents = array(
  array('title' => 'About CREA', 'body' => $body,  
                  'menu' => array( 'parent' => 0, 'weight' => 1 )),

  array('title' => 'People', 'body' => $body, 
                  'menu' => array('parent' => 0, 'weight' => 2 )),

  array('title' => 'Research', 'body' => $body, 
                  'menu' => array('parent' => 0, 'weight' => 3 )),

  array('title' => 'Singapore Life Panel', 'body' => $body, 
                  'menu' => array('parent' => 0, 'weight' => 4)),

  array('title' => 'Resources', 'body' => $body, 
                  'menu' => array('parent' => 0, 'weight' => 5 )),

  array('title' => 'Objectives of CREA + Importance of CREA', 'body' => $body, 
                  'menu' => array('parent' => 'About CREA', 'weight' => 0)),
  array('title' => 'Message from the Director', 'body' => $body, 
                  'menu' => array('parent' => 'About CREA', 'weight' => 1 )),

  array('title' => 'Key Members of Reasearch Team', 'body' => $body, 
                  'menu' => array('parent' => 'People', 'weight' => 0 )),
  array('title' => 'CREA Staff Profiles', 'body' => $body, 
                  'menu' => array('parent' => 'People', 'weight' => 1 )),

  array('title' => 'Background to Research', 'body' => $body, 
                  'menu' => array('parent' => 'Research', 'weight' => 0 )),
  array('title' => 'Research Methodology', 'body' => $body, 
                  'menu' => array('parent' => 'Research', 'weight' => 1 )),
  array('title' => 'Research Findings', 'body' => $body, 
                  'menu' => array('parent' => 'Research', 'weight' => 2 )),

  array('title' => 'Key Contact Point', 'body' => $body, 
                  'menu' => array('parent' => 'Singapore Life Panel', 'weight' => 0 )),
  array('title' => 'Importance of SLP to Research', 'body' => $body, 
                  'menu' => array('parent' => 'Singapore Life Panel', 'weight' => 1 )),
  array('title' => 'Methods of Survey', 'body' => $body, 
                  'menu' => array('parent' => 'Singapore Life Panel', 'weight' => 2 )),
  array('title' => 'Link to Survey', 'body' => $body, 
                  'menu' => array('parent' => 'Singapore Life Panel', 'weight' => 3 )),

  array('title' => 'Publications from Research', 'body' => $body, 
                  'menu' => array('parent' => 'Resources', 'weight' => 0 )),
  array('title' => 'Link to related publications', 'body' => $body, 
                  'menu' => array('parent' => 'Resources', 'weight' => 1 )),
                

  );
  foreach ($contents as $key => $content) {
    
    if (_crea_check_if_title_exist($content['title'], 'page')) {
      continue;
    }

    $content['body'] = file_get_contents('http://loripsum.net/api/10/long/decorate/bq/ul/ol/link/prude');
    crea_create_node($content['title'], 'page', $content['body'], $content['menu']);
  }
}

function crea_create_node($title, $type, $body, $menu = FALSE) {

  $node = new stdClass(); 
  $node->type = $type; 
  $node->language = LANGUAGE_NONE; 
  node_object_prepare($node);

  $node->title = $title;
  $node->body[$node->language][0]['value'] = $body;
  $node->body[$node->language][0]['summary'] = '';
  $node->body[$node->language][0]['format'] = 'full_html';

  $node->status = 1;   // (1 or 0): published or unpublished
  $node->promote = 0;  // (1 or 0): promoted to front page or not
  $node->sticky = 0;  // (1 or 0): sticky at top of lists or not
  $node->comment = 1;  // 2 = comments open, 1 = comments closed, 0 = comments hidden
  // Add author of the node
  $node->uid = 1;
  $node->pathauto_perform_alias = TRUE;
  $node->path['pathauto'] = TRUE;

  if ($menu !== FALSE) {

    $plid = 0;
    if ($menu['parent'] !== 0 || is_string($menu['parent'])) {
      $plid = _crea_get_mlid($menu['parent']);
    }

    $node->menu = array(
      'enabled' => 1,
      'mlid' => 0,
      'module' => 'menu',
      'hidden' => 0,
      'has_children' => 0,
      'customized' => 0,
      'options' => array(),
      'expanded' => 1,
      'parent_depth_limit' => 8,
      'link_title' => $title,
      'description' => '',
      'parent' => 'main-menu:' . $plid,
      'weight' => $menu['weight'],
      'plid' => $plid,
      'menu_name' => 'main-menu'
    );

  }

  // Save the node
  node_save($node);
}

function _crea_get_mlid($menu_title) {

  $mlid = null;
  $tree =  menu_tree_all_data('main-menu');

  foreach($tree as $item) {
    if ($item['link']['title'] == $menu_title) {
      $mlid = $item['link']['mlid'];
      break;
    }
  }

  return $mlid;
}

function _crea_check_if_title_exist($title, $bundle = null, $excluded_nids = null) {

  $query = new EntityFieldQuery;
  $query
    ->entityCondition('entity_type', 'node')
    ->propertyCondition('title', $title);
  if (!empty($bundle)) {
    $query->entityCondition('bundle', $bundle);
  }
  if (!empty($excluded_nids)) {
    $query->propertyCondition('nid', $excluded_nids, is_array($excluded_nids) ? 'NOT IN' : '!=');
  }
  return (bool) $query->range(0, 1)->count()->execute();
}

function _crea_get_node_by_title($title, $type) {
  $query = new EntityFieldQuery();
  $node = NULL;

  $entities = $query->entityCondition('entity_type', 'node')
  ->propertyCondition('type', $type)
  ->propertyCondition('title', $title)
  ->propertyCondition('status', 1)
  ->range(0,1)
  ->execute();

  if (!empty($entities['node'])) {
    $keys = array_keys($entities['node']);
    $nid = array_shift($keys);
    $node = node_load($nid);
  }

  return $node;
}

function _crea_menu_item_setup ($items) {

  $whatsnew_title = t("What's New");
  $items['whats-new'] = array(
    'title' => $whatsnew_title,
    'description' => $whatsnew_title,
    'page callback' => 'crea_core_content_type_page',
    'page arguments'   => array('events'),
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
    'expanded' => TRUE,
    'menu_name' => 'main-menu',
    'weight' => 20,
  );
  
  if (isset($items['events'])) {
    $plid = _crea_get_mlid($whatsnew_title);
    $items['events']['type'] = MENU_NORMAL_ITEM;
    $items['events']['menu_name'] = 'main-menu';
    $items['events']['weight'] = 0;
    $items['events']['plid'] = $plid;
  }

  if (isset($items['news'])) {
    $plid = _crea_get_mlid($whatsnew_title);
    $items['news']['type'] = MENU_NORMAL_ITEM;
    $items['news']['menu_name'] = 'main-menu';
    $items['news']['weight'] = 1;
    $items['news']['plid'] = $plid;
  }

  if (isset($items['articles'])) {
    $plid = _crea_get_mlid('Research');
    $items['articles']['title'] = t('Articles from Novus');
    $items['articles']['type'] = MENU_NORMAL_ITEM;
    $items['articles']['menu_name'] = 'main-menu';
    $items['articles']['weight'] = 20;
    $items['articles']['plid'] = $plid;
  }

  return $items;
}


