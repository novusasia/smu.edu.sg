<?php
/**
 * @file
 * crea_core.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function crea_core_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-crea-latest_news'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'crea-latest_news',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'triptych_first',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'crea' => array(
        'region' => 'main_lower',
        'status' => 1,
        'theme' => 'crea',
        'weight' => -15,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-crea-recent_articles'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'crea-recent_articles',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'crea' => array(
        'region' => 'content_bottom',
        'status' => 1,
        'theme' => 'crea',
        'weight' => -15,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-crea-recent_events'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'crea-recent_events',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'crea' => array(
        'region' => 'main_lower',
        'status' => 1,
        'theme' => 'crea',
        'weight' => -14,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-crea-resources'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'crea-resources',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'crea' => array(
        'region' => 'main_lower',
        'status' => 1,
        'theme' => 'crea',
        'weight' => -13,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
