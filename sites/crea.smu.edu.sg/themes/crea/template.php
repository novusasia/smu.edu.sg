<?php
/**
 * @file
 * The primary PHP file for the CREA theme.
 *
 * This file should only contain light helper functions and point to stubs in
 * other files containing more complex functions.
 *
 * The stubs should point to files within the `./includes` folder named after
 * the function itself minus the theme prefix. If the stub contains a group of
 * functions, then please organize them so they are related in some way and name
 * the file appropriately to at least hint at what it contains.
 *
 * All [pre]process functions, theme functions and template files lives inside
 * the `./templates` folder. This is a highly automated and complex system
 * designed to only load the necessary files when a given theme hook is invoked.
 *
 */

function crea_preprocess_region(&$vars) {

    $region_name = $vars['region'];
    $blocks = block_get_blocks_by_region($region_name);
    unset($blocks['#sorted']);
    $num_of_blocks = count($blocks);
    $m_grid = 12;

    if ($num_of_blocks > 1) {
        $m_grid = $num_of_blocks < 6 ? round(12 / $num_of_blocks) : $m_grid;
        $markup = '';
        foreach($blocks as $key => $block) {
            $markup .= '<div class="col-md-'.$m_grid.' columns">' . $vars['elements'][$key]['#children'] .'</div>';
        }
        $vars['content'] = '<div class="row equal-heights">'.$markup.'</div>';
    }
}

function crea_preprocess_views_view(&$vars) {
    array_unshift($vars['classes_array'], "row");
}

function crea_preprocess_views_view_unformatted(&$vars) {
    $view = $vars['view'];
    $rows = $vars['rows'];
    $num_of_rows = count($rows);
    $m_grid = 12;

    /* Recent Articles */
    if ($view->current_display == 'recent_articles') {
        $vars['classes'] = array();
        $m_grid = $num_of_rows < 6 ? round(12 / $num_of_rows) : $m_grid;
        foreach ($rows as $id => $row) {
            $vars['classes'][$id][] = 'views-column';
            $vars['classes'][$id][] = 'col-md-' . $m_grid;
            $vars['classes'][$id][] = 'col-sm-' . $m_grid;
            $vars['classes_array'][$id] = isset($vars['classes'][$id]) ? implode(' ', $vars['classes'][$id]) : '';
        } 
    }
}

function crea_preprocess_page(&$vars) {

    $vars['row_fluid'] = 'row';
    $vars['part12'] = 'span12';

    if (empty($vars['bootstrap_version']) || empty(theme_get_setting('bootstrap_version'))) {
        $theme_settings = variable_get('theme_crea_settings', array());
        $theme_settings['bootstrap_version'] = 'bootstrap-3';
        $theme_settings['font_awesome_version'] = 'font-awesome-4';
        variable_set('theme_crea_settings', $theme_settings);
    }

    if ($vars['is_front'] && arg(0) == 'node') {
        $vars['page']['content'] = '';
        $vars['feed_icons'] = '';
    }

    if (!isset($vars['page']['announcement'])) {
        $vars['page']['announcement'] = '';
    }

    if (!isset($vars['page']['lang'])) {
        $vars['page']['lang'] = '';
    }

    if (isset($vars['node']) && node_is_page($vars['node'])) {
        $vars['title'] = '';
    }

    if (drupal_is_front_page()) {
        $vars['title'] = '';
    }

    $node = menu_get_object('node');
    if ($node) {
        $exclude_type = array('event', 'resources');
        if(!in_array($node->type, $exclude_type)) {
            $image = field_get_items('node', $node, 'field_image');
            if($image) {
                $image_instance = field_info_instance('node', 'field_image', $node->type);
                $image_display = field_get_display($image_instance, 'full', $node);
                $output = field_view_value('node', $node, 'field_image', $image[0], array(
                  'type' => 'image',
                  'settings' => array(
                    'image_style' => $image_display['settings']['image_style'],
                    'image_link' => $image_display['settings']['image_link'],
                  ),
                ));
                $image_banner = '<div class="banner-image">'.drupal_render($output).'</div>';
                $vars['page']['main_top']['field_image']['#markup'] = $image_banner;

            }
        }
    }

    if(isset($vars['feed_icons'])) {
        $vars['feed_icons'] = '';
    }

}

function crea_preprocess_node(&$vars) {

    /* Create template for view modes */
    $vars['theme_hook_suggestions'][] = 'node__' . $vars['view_mode'];
    $vars['theme_hook_suggestions'][] = 'node__' . $vars['node']->type . '_' . $vars['view_mode'];
}

function crea_css_alter(&$css) { 

    unset($css[drupal_get_path('module','system').'/system.base.css']); 
    unset($css[drupal_get_path('module','system').'/system.menus.css']);
    unset($css[drupal_get_path('module','system').'/system.theme.css']);
    unset($css[drupal_get_path('module','system').'/system.messages.css']);
    unset($css[drupal_get_path('module','node').'/node.css']);
    unset($css[drupal_get_path('module','field').'/theme/field.css']);
    unset($css[drupal_get_path('module','user').'/user.css']);
    unset($css[drupal_get_path('module','search').'/search.css']);
    unset($css[drupal_get_path('module','views').'/css/views.css']);
    unset($css[drupal_get_path('module','ckeditor').'/css/ckeditor.css']);
    unset($css[drupal_get_path('module','ctools').'/css/ctools.css']);
    unset($css[drupal_get_path('module','locale').'/locale.css']);
    unset($css[drupal_get_path('module','taxonomy').'/taxonomy.css']);
    unset($css[drupal_get_path('module','webform').'/css/webform.css']);
    unset($css[drupal_get_path('theme','open_framework').'/packages/bootstrap-2.3.1/css/bootstrap.min.css']);
    unset($css[drupal_get_path('theme','open_framework').'/packages/bootstrap-2.3.1/css/bootstrap-responsive.min.css']);

    $tmp = $css;
    foreach($tmp as $file => $detail) {
        $file = ltrim($file, '/');
        if(!file_exists($file)) {
            unset($css[$file]);
        }
    }

}

function crea_preprocess_html(&$vars) {

    drupal_add_css(drupal_get_path('theme', 'smubase') . '/packages/bootstrap/css/bootstrap.min.css', array('group' => CSS_DEFAULT, 'media' => 'all', 'weight' => 500, 'preprocess' => TRUE));
    drupal_add_js(drupal_get_path('theme', 'smubase') . '/packages/bootstrap/js/bootstrap.min.js');
    drupal_add_js(drupal_get_path('theme', 'crea') . '/js/script.js',
                     array('type' => 'file', 'scope' => 'footer'));

    if(!isset($vars['page_layout'])) {
        $vars['page_layout'] = '';
    }

    if(!isset($vars['bootstrap_version'])) {
        $vars['bootstrap_version'] = 'bootstrap-3';
    }

    

}

function crea_js_alter(&$js) {

    unset($js[drupal_get_path('theme','open_framework').'/packages/bootstrap-2.3.1/js/bootstrap.min.js']);
    unset($js['//use.typekit.com/uhs0req.js']);

    $tmp = $js;
    foreach($tmp as $file => $detail) {
        $file = ltrim($file, '/');
        if(!file_exists($file)) {
            unset($js[$file]);
        }
    }
}


