
<style>
/*
#block-crea-crea-msg {
	background-image: linear-gradient(-90deg, #18AAB0 0%, #FFFFFF 100%);
	padding: 10px 0;
}

#block-crea-crea-msg p, 
#block-crea-crea-msg .block-title, 
#block-crea-crea-msg a {
	color: #FFFFFF;
}

#block-crea-crea-msg .content {
	padding-top:20px;
}

#block-crea-crea-msg .featured_image {
	padding: 20px;
}

#block-crea-crea-msg .circle {
	position: absolute;
	right:0;
	bottom:-20px;
	z-index: 1;
	width: 60px;
	height: 60px;
	background: red;
	-moz-border-radius: 100px / 100px;
	-webkit-border-radius: 100px / 100px;
	border-radius: 100px / 100px;
	background: #18AAB0;
	opacity: 0.8;
}

@media (max-width: 767px) { 
	#block-crea-crea-msg {
		background-image: linear-gradient(0deg, #18AAB0 0%, #FFFFFF 100%);
		margin: 25px;
		padding: 0;
	}
}

@media only screen and (min-width : 768px) {
	#block-crea-crea-msg {
		padding:20px;
		background-image: linear-gradient(0deg, #18AAB0 0%, #FFFFFF 100%);
	}
}

@media only screen and (min-width : 992px) {
	#block-crea-crea-msg {
		background-image: linear-gradient(-90deg, #18AAB0 0%, #FFFFFF 100%);
	}
}
*/

#block-crea-core-crea-msg {
	visibility: hidden;
}

#block-crea-core-crea-msg .content {
	position: relative;
	background-image: linear-gradient(-90deg, #18AAB0 0%, #FFFFFF 100%);
	padding: 20px;
	color: #FFFFFF;
}

#block-crea-core-crea-msg a,
#block-crea-core-crea-msg h1,
#block-crea-core-crea-msg h2,
#block-crea-core-crea-msg h3,
#block-crea-core-crea-msg h4,
#block-crea-core-crea-msg h5,
#block-crea-core-crea-msg h6,
#block-crea-core-crea-msg .content p {
	color: #FFFFFF;
}

#block-crea-core-crea-msg .featured_image {
	z-index: 1;
}
/*
@media (max-width: 767px) { 
	#block-crea-core-crea-msg {
		background-image: linear-gradient(0deg, #18AAB0 0%, #FFFFFF 100%);
	}

	#block-crea-core-crea-msg .content {
		background: transparent;
	}
}

@media only screen and (min-width : 768px) {
	#block-crea-core-crea-msg {
		background-image: linear-gradient(0deg, #18AAB0 0%, #FFFFFF 100%);
	}

	#block-crea-core-crea-msg .content {
		background: transparent;
	}
}

@media only screen and (min-width : 992px) {
	#block-crea-core-crea-msg .content {
		background-image: linear-gradient(-90deg, #18AAB0 0%, #FFFFFF 100%);
	}
}
*/
</style>

<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
	<div class="featured_image col-md-5">
			<?php if ($block->featured_image): ?>
			<?php print $block->featured_image ?>
			<?php endif;?>
	</div>

	<div class="content" <?php print $content_attributes; ?>>
		<?php print render($title_prefix); ?>
		<?php if ($block->subject): ?>
			<h2 class="block-title"<?php print $title_attributes; ?>><?php print $block->subject ?></h2>
		<?php endif;?>
		<?php print render($title_suffix); ?>
		<?php print $content ?>
		<?php if($block->link_url): ?>
		<a href="<?php print $block->link_url; ?>" class="read-more"><?php print $block->link_label; ?></a>
		<?php endif; ?>
	</div>
</div>

<script type="text/javascript">
	jQuery(function() {
		blockHeight =jQuery("#<?php print $block_html_id; ?> .featured_image").outerHeight();
		contentHeight = jQuery("#<?php print $block_html_id; ?> .content").outerHeight();

		if (blockHeight > contentHeight) {
			topSize = (blockHeight - contentHeight) / 2;
			jQuery("#<?php print $block_html_id; ?> .content").css( "top", topSize );
		} else {
			topSize = (contentHeight - blockHeight) / 2;
			jQuery("#<?php print $block_html_id; ?> .featured_image").css( "top", topSize );

		}
		jQuery("#<?php print $block_html_id; ?>").css( "visibility", "visible" );
	});
</script>
	

