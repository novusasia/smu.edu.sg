
<style type="text/css">

	#block-crea-core-crea-staticbanner .content {
		padding:20px 40px;
	}


	#block-crea-core-crea-staticbanner .rectangle {
		position: absolute;
		top:0;
		z-index: 1;
		width: 60%;
		height: 90px;
		background: #18AAB0;
		opacity: 0.8;
	}

	#block-crea-core-crea-staticbanner .square {
		position: absolute;
		right:15px;
		top:-200px;
		z-index: 1;
		width: 230px;
		height: 230px;
		background: #18AAB0;
		opacity: 0.8;
	}

	#block-crea-core-crea-staticbanner .circle {
		position: absolute;
		left:15px;
		top:50px;
		z-index: 1;
		width: 60px;
		height: 60px;
		background: red;
		-moz-border-radius: 100px / 100px;
		-webkit-border-radius: 100px / 100px;
		border-radius: 100px / 100px;
		background: #18AAB0;
		opacity: 0.8;
	}


	@media (max-width: 767px) { 
		#block-crea-core-crea-staticbanner .featured_image {
			padding: 0;
		}

		#block-crea-core-crea-staticbanner .content {
			padding:0;
			margin: 0;
			padding-left: 15px;
		}

		#block-crea-core-crea-staticbanner .content p {
			font-size: 32px;
			line-height: 38px;
		}

		#block-crea-core-crea-staticbanner .rectangle {
			height: 35px;
		}

		#block-crea-core-crea-staticbanner .square {
			width: 80px;
			height: 80px;
			top:-40px;
			right: 0;
		}

		#block-crea-core-crea-staticbanner .circle {
			width: 30px;
			height: 30px;
			left:20px;
		}
	}

	@media only screen and (min-width : 768px) {
		#block-crea-core-crea-staticbanner .rectangle {
			height: 50px;
		}

		#block-crea-core-crea-staticbanner .square {
			top:-100px;
			width: 170px;
			height: 170px;
		}

		#block-crea-core-crea-staticbanner .circle {
			width: 40px;
			height: 40px;
			left:5px;
			top:90px;
		}
	}

	@media only screen and (min-width : 992px) {
		#block-crea-core-crea-staticbanner .square {
			width: 200px;
			height: 200px;
		}

		#block-crea-core-crea-staticbanner .circle {
			left:25px;
			top:120px;
		}
	}

</style>

<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

	<div class="featured_image col-md-12">
		<?php if ($block->featured_image): ?>
		<?php print $block->featured_image ?>
		<?php endif;?>
		<div class="rectangle"></div>
	</div>

	<div class="content col-xs-7 col-md-7" <?php print $content_attributes; ?>>
		<?php print $content ?>
	</div>
	<div class=" col-xs-offset-2 col-xs-3 col-sm-offset-2 col-sm-3 col-md-offset-2 col-md-3">
		<div class="square"></div>
		<div class="circle"></div>
	</div>
</div>

