
<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

	<div class="featured_image col-xs-11 col-sm-10 col-md-6">
		<?php if ($block->featured_image): ?>
		<?php print $block->featured_image ?>
		<?php endif;?>
	</div>

	<div class="content col-sm-12 col-md-6" <?php print $content_attributes; ?>>
		<?php print render($title_prefix); ?>
		<?php if ($block->subject): ?>
			<h2 class="block-title"<?php print $title_attributes; ?>><?php print $block->subject ?></h2>
		<?php endif;?>
		<?php print render($title_suffix); ?>
		<div class="content-wrapper"><?php print $content ?></div>
		<?php if($block->link_url): ?>
		<a href="<?php print $block->link_url; ?>" class="read-more"><?php print $block->link_label; ?></a>
		<?php endif; ?>
	</div>
</div>

