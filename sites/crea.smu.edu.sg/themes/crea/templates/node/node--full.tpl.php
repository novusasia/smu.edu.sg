
<?php hide($content['field_image']); ?>
<?php $content_col = 'col-md-12' ?>
<div class="page-full-container ">
	<?php if(isset($content['field_quote'])): ?>
	<div class="page-full-quote col-md-4">
		<span class="circle"></span>
		<span class="circle"></span>
		<?php print render($content['field_quote']); ?>
		<?php $content_col = 'col-md-6' ?>
	</div>
	<?php endif; ?>
	<div class="page-full-content <?php print $content_col; ?>">
		<h1<?php print $title_attributes; ?>> <?php print $title; ?></h1>
		<?php print render($content); ?>
	</div>
</div>