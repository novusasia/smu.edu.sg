
<div class="page-container row">
  <div class="page-content col-md-8">
    <h1<?php print $title_attributes; ?>>
      <?php print $title; ?>
    </h1>
    <?php print render($content['field_event_description']); ?>
    <?php print render($content['field_image']); ?>
  </div>
  <div class="event-sidebar col-md-4">
    <div class="event-sidebar-top">
      <?php if($content['field_event_date']): ?>
    	<h3>WHEN</h3>
    	<?php print render($content['field_event_date']); ?>
      <?php endif; ?>
      <?php if($content['field_event_venue']): ?>
    	<h3>VENUE</h3>
    	<?php print render($content['field_event_venue']); ?>
      <?php endif; ?>
      <?php if($content['field_event_sponsor']): ?>
      <h3>Speakers</h3>
      <?php print render($content['field_event_speaker']); ?>
      <?php endif; ?>
    </div>
    <div class="event-sidebar-bottom">
      <?php if($content['field_event_sponsor']): ?>
      <h3>Sponsors</h3>
      <?php print render($content['field_event_sponsor']); ?>
      <?php endif; ?>
    </div>
  </div>
</div>