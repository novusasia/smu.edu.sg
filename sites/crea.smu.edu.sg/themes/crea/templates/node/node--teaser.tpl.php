
<div class="teaser-container">
  <?php $content_col = 'col-md-12'; ?>
  <?php if (isset($content['field_image'])): ?>
  <?php $content_col = 'col-md-8'; ?>
  <div class="teaser-image col-md-4">
    <?php print render($content['field_image']); ?>
  </div>
  <?php endif; ?>

  <?php if (isset($content['field_news_featured'])): ?>
  <?php $content_col = 'col-md-8'; ?>
  <div class="teaser-image col-md-4">
    <?php print render($content['field_news_featured']); ?>
  </div>
  <?php endif; ?>

  <?php if (isset($content['field_event_photo'])): ?>
  <?php $content_col = 'col-md-8'; ?>
  <div class="teaser-image col-md-4">
    <?php print render($content['field_event_photo']); ?>
  </div>
  <?php endif; ?>
  <div class="teaser-content <?php print $content_col; ?>">
    <h2 class="teaser-title"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php hide($content['links']); ?>
    <?php print render($content); ?>
    <a href="<?php print $node_url; ?>" class="view-more">Read More</a>
  </div>
</div>
