<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in html.tpl.php and page.tpl.php.
 * Some may be blank but they are provided for consistency.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 *
 * @ingroup themeable
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>

  <style type="text/css">
    #page {
      text-align: center;
      margin: 0 auto;
      margin-top:150px;
    }

    #nav-logo {
      margin: 50px;
    }

    .smu-logo {
      float: none;
    }
  </style>
</head>
<body class="<?php print $classes; ?>">
  <div id="page">
    <div id="header">

        <div id="nav-logo">
            <?php if ($logo): ?>
            <span class="smu-logo  hidden-xs"><a target="_blank" href="http://www.smu.edu.sg" rel="home" title="Singapore Management University (SMU)" name="top"><img id="smulogo" alt="Singapore Management University (SMU)" src="/sites/all/themes/smubase/smu_logo.png"></a></span>
            <span><a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
              <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
            </a></span>
            <?php endif; ?>
            </div>
        </div>

      <div id="content">
          <?php if (!empty($title)): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
          <?php if (!empty($messages)): print $messages; endif; ?>
          <div id="content-content" class="clearfix">
            <?php print $content; ?>
          </div> <!-- /content-content -->
        </div> <!-- /content -->

    </div> <!-- /header -->

  </div> <!-- /page -->

</body>
</html>
