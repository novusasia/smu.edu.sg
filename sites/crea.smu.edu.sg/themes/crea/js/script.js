
(function ($) {

    Drupal.behaviors.crea = {
        attach: function (context, settings) {

          $(window).load(function() {
            setEqualHeights();
            setTeaserContentHeight();
            $(".view-more, .more-link a, .read-more").css( "visibility", "visible" ).hide().fadeIn('slow');
          });

          
          $(window).resize(function() {
            setEqualHeights();
            setTeaserContentHeight();
          });

          function setEqualHeights() {
            var equalheight = 0;
            var colHeight = 0;
            $('.equal-heights').children('.columns').each(function(i) { 
              colHeight = $(this).outerHeight();
              if (colHeight > equalheight) {
                equalheight = colHeight;
              }
            });

            $(".equal-heights > .columns > div").css( "height", equalheight );
          }

          function setTeaserContentHeight() {
            imageHeight = $(".teaser-container .teaser-image").outerHeight();
            contentHeight = $(".teaser-container .teaser-content").outerHeight();

            if (imageHeight > contentHeight) {
              $(".teaser-container .teaser-content").css( "height", imageHeight );
            } else {
              $(".teaser-container .view-more").css( "position", "relative");
              $(".teaser-container .view-more").css( "display", "inline-block");
            }
          }
      }

    };

} (jQuery));